//
//  NSCoderMock.m
//  Models
//
//  Created by Daniel García on 27/10/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "NSCoderMock.h"
@interface NSCoderMock()

@end
@implementation NSCoderMock
- (void)encodeObject:(id)objv forKey:(NSString *)key{
    self.numberOfCalls ++;
}
- (void)encodeFloat:(CGFloat)number forKey:(NSString *)key{
    self.floatNumberOfCalls++;
}
@end
