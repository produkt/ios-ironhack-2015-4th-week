//
//  ShowsTests.m
//  Models
//
//  Created by Daniel García on 27/10/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#define HC_SHORTHAND
#import <OCHamcrest/OCHamcrest.h>

#define MOCKITO_SHORTHAND
#import <OCMockito/OCMockito.h>


#import "Show.h"
#import "NSCoderMock.h"

@interface ShowsTests : XCTestCase

@end

@implementation ShowsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testShowCopy {
    Show *show = [[Show alloc] init];
    show.showTitle = @"True Detective";
    show.showId = @"1";
    show.showDescription = @"Awesome show !";
    show.showRating = 9.0;
    
    Show *showCopy = [show copy];
    
    XCTAssertEqualObjects(show.showId, showCopy.showId);
    XCTAssertEqualObjects(show.showTitle, showCopy.showTitle);
    XCTAssertEqualObjects(show.showDescription, showCopy.showDescription);
    XCTAssertEqual(show.showRating, showCopy.showRating);

}

- (void)testIsEqualToShow {
    Show *show = [[Show alloc] init];
    show.showTitle = @"True Detective";
    show.showId = @"1";
//    show.showDescription = @"Awesome show !";
//    show.showRating = 9.0;
    
    Show *secondShow = [[Show alloc] init];
    secondShow.showTitle = @"True Detective";
    secondShow.showId = @"1";
//    secondShow.showDescription = @"";
//    secondShow.showRating = 10.0;
    
//    XCTAssertTrue([show isEqualToShow:secondShow]);
    XCTAssertEqual([show hash], [secondShow hash]);
}

- (void)testShowsNSCoding {
    NSCoder *nscoderMock = mock([NSCoder class]);
    Show *show = [[Show alloc] init];
    show.showTitle = @"True Detective";
    show.showId = @"1";
    show.showDescription = @"Awesome show !";
    show.showRating = 9.0;
    [show encodeWithCoder:nscoderMock];
    [verify(nscoderMock) encodeObject:show.showId forKey:@"showId"];
    [verify(nscoderMock) encodeObject:show.showTitle forKey:@"showTitle"];
    [verify(nscoderMock) encodeObject:show.showDescription forKey:@"showDescription"];
    [verify(nscoderMock) encodeObject:@(show.showRating) forKey:@"showRating"];
}
- (void)testShowsInitWithCoder {
    NSCoder *nscoderMock = mock([NSCoder class]);
    Show *show = [[Show alloc] initWithCoder:nscoderMock];
    [verify(nscoderMock) decodeObjectForKey:@"showId"];
    [verify(nscoderMock) decodeObjectForKey:@"showTitle"];
    [verify(nscoderMock) decodeObjectForKey:@"showDescription"];
    [verify(nscoderMock) decodeObjectForKey:@"showRating"];
}

@end




