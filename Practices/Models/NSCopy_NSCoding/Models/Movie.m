//
//  Movie.m
//  Models
//
//  Created by Daniel García García on 18/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "Movie.h"

@implementation Movie

@end

//
//@implementation Movie(NSCopying)
//- (id)copyWithZone:(NSZone *)zone{
//    Movie *movieCopy = [[[self class] allocWithZone:zone] init];
//    if (movieCopy) {
//        movieCopy.movieId = [self.movieId copyWithZone:zone];
//        movieCopy.movieDescription = [self.movieDescription copyWithZone:zone];
//        movieCopy.movieTitle = [self.movieTitle copyWithZone:zone];
//        movieCopy.movieRating = self.movieRating;
//    }
//    return movieCopy;
//}
//@end
//
//@implementation Movie(NSCoding)
//- (id)initWithCoder:(NSCoder *)aDecoder{
//    self = [super init];
//    if (self) {
//        self.movieId = [aDecoder decodeObjectForKey:@"movieId"];
//        self.movieDescription = [aDecoder decodeObjectForKey:@"movieDescription"];
//        self.movieTitle = [aDecoder decodeObjectForKey:@"movieTitle"];
//        self.movieRating = [aDecoder decodeFloatForKey:@"movieRating"];
//        
//    }
//    return self;
//}
//- (void)encodeWithCoder:(NSCoder *)aCoder{
//    [aCoder encodeObject:self.movieId forKey:@"movieId"];
//    [aCoder encodeObject:self.movieDescription forKey:@"movieDescription"];
//    [aCoder encodeObject:self.movieTitle forKey:@"movieTitle"];
//    [aCoder encodeFloat:self.movieRating forKey:@"movieRating"];
//
//}
//@end