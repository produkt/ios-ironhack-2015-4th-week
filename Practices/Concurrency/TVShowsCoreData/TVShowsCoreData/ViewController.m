//
//  ViewController.m
//  TVShowsCoreData
//
//  Created by Daniel García on 28/10/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "ViewController.h"
#import "ShowsProvider.h"
#import "ShowEntity.h"
#import "DetailViewController.h"
#import "ShowsTableViewCell.h"
#import "UIImageView+IHWebImage.h"


@interface ViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic) ShowsProvider *showsProvider;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *shows;
@end

@implementation ViewController
- (NSArray *)loadCachedShows{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([ShowEntity class])];
    fetchRequest.predicate = [NSPredicate predicateWithValue:YES];
    NSArray *shows = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    return shows;
}
- (ShowsProvider *)showsProvider{
    if (!_showsProvider) {
        _showsProvider = [[ShowsProvider alloc] init];
    }
    return _showsProvider;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.shows = [self loadCachedShows];
    if (self.shows.count == 0) {
        
        [self.showsProvider loadShowsInManagedObjectContext:self.managedObjectContext withCompletionBlock:^(NSArray *shows) {
            self.shows = shows;
            [self.tableView reloadData];
        }];
    }
    [self saveTVShowsNamesPList];
}

- (void)saveTVShowsNamesPList{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths firstObject];
    NSString *plistPath = [cachePath stringByAppendingPathComponent:@"shows.plist"];
    
    
    NSMutableArray *showsNames = [NSMutableArray array];
    for (ShowEntity *show in self.shows) {
        [showsNames addObject:show.showName];
    }
    [showsNames writeToFile:plistPath atomically:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.shows.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    
    static NSString *cellIdentifier = @"ShowsTableViewCell";
    ShowsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[[UINib nibWithNibName:cellIdentifier bundle:nil] instantiateWithOwner:self options:nil] firstObject];
    }
    
    ShowEntity *show = [self.shows objectAtIndex:indexPath.row];
    cell.showTitleLabel.text = show.showName;
    NSURL *imageURL = [NSURL URLWithString:show.showURL];
    
    [cell.showBannerImageView setImageFromURL:imageURL withCompletionBlock:^(UIImage *image) {
        
    }];
    
    return cell;
}
+ (ViewController *)singleton{
    static ViewController *vc;
    if (!vc) {
        vc = [[ViewController alloc] init];
    }
    return vc;
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ShowEntity *show = [self.shows objectAtIndex:indexPath.row];
    DetailViewController *detailVC = [[DetailViewController alloc]initWithShow:show];
    [self.navigationController pushViewController:detailVC animated:YES];    
}
@end
