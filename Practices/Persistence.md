# Persistence

## Exercise 1 (Shows App with a User Login on Core Data)

- Implement a basic Core Data stack on a separated CoreDataManager class (don't use the XCode AppDelegate template stack)

- Create a simply Core Data model (xcdatamodeld) with a UserEntity

- Implement a user login view controller

- Check if any user is logged in the application. If not, present login VC

- Implement a user profile view controller and implement logout feature

- Each TVShows and Movies sections must show logged username on section title 

- Listen MOC changes notification and update section titles when user login occurs


## Exercise 2

- Use StandardUserDefaults store the last time a user started a session

- Ensure data in StandardUserDefaults is saved to disk when application exits

- Log in the console the last time a user started a session


## Exercise 2 

- Implement "like" movie and "like" tvshow features

- Use a shared NSDictionary to store the likes (save NSDictionaries with showId and likesCount pairs)

- Save "likes" into a dictionary and store into a plist

- Read that plist and draw the likes counter for each show



## Exercise 3

- If user logs out, all cached data must be deleted

- On each application startup , log all cache files metadata





