#Persistence

## IronHack iOS Bootcamp 
##![100%,inline](assets/ironhacklogo.png)
### Week 4 - Day 2


####Daniel García - Produkt
---

#Core Data
##![20%,inline,original](assets/coredata.png)
---

#Core Data
##ORM (Object-Relational Mapping)
##Object oriented data storage abstraction layer

---

##SQLite (default) , XML , Binary… 

---

![left,original](assets/single_persistent_stack.jpg)

##Core Data
##Stack

---

#NSManagedObject 
#& 
#NSManagedObjectContext

---

#NSManagedObjets 
##Entities concretions defined by the data model
###Live in memory attached to an NSManagedObjectContext

---

#NSManagedObjectContexts
##Manage every NSManagedObject and all its CRUD operations
###They are not thread-safe

---

#Managed Object Model
## Defined by awesomeApp.xcdatamodeld

#![inline,original,80%](assets/xcdatamodel.png)

---

#Create a new entity (Crud)

```objectivec
NSManagedObjectContext *managedObjectContext=[[NSManagedObjectContext alloc]init];
UserEntity *user = [NSEntityDescription insertNewObjectForEntityForName:@"UserEntity" 
inManagedObjectContext:managedObjectContext];
user.userId=@"1";
user.userName=@"John Appleseed";

NSError *error;
[managedObjectContext save:&error];
```
---
#Read an entity (cRud)

```objectivec
- (UserEntity *)userWithId:(NSString *)userId 
inManagedObjectContext:(NSManagedObjectContext*)managedObjectContext{

	NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"UserEntity"];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@“userName = %@“, userId];   
	NSError *error;
	NSArray *fetchResult=[managedObjectContext executeFetchRequest:fetchRequest error:&error];
	return fetchResult.count?[fetchResult firstObject]:nil;

}
```

---
#Update an entity (crUd)

```objectivec
NSManagedObjectContext *managedObjectContext=[[NSManagedObjectContext alloc]init];
UserEntity *user = [self userWithId:@"1" inManagedObjectContext:managedObjectContext];

user.name=@"John Doe";

NSError *error;
[managedObjectContext save:&error];
```

---
#Delete an entity (cruD)
```objectivec
NSManagedObjectContext *managedObjectContext=[[NSManagedObjectContext alloc]init];
UserEntity *user = [self userWithId:@"1" inManagedObjectContext:managedObjectContext];

[managedObjectContext deleteObject:user];

NSError *error;
[managedObjectContext save:&error];
```

--- 

#PRACTICE

--- 

#NSUserDefaults
##Storage interface for user preferences (key-value)

--- 

##It is not designed to store application's data. It should be only used to store user preferences, configuration data, etc

--- 

#Write in NSUserDefaults

```objectivec
NSString *currentLang=@"es";
[[NSUserDefaults standardUserDefaults]setObject:currentLanguage 
										 forKey:@"userConfigCurrentLang"];
```

##'object' must be any of `NSData`, `NSString`, `NSNumber`, `NSDate`, `NSArray`, or `NSDictionary` type
###when storing a collection, all it's containing objects must be also of one of these allowed types

--- 

##Special data writing methods (NSUserDefaults)

```objectivec
setBool:forKey:
setFloat:forKey:
setInteger:forKey:
setDouble:forKey:
setURL:forKey:
```
--- 

##Read from NSUserDefaults

```objectivec
NSString *currentLang;
currentLang = [[NSUserDefaults standardUserDefaults] 
			  objectForKey:@"userConfigCurrentLang"];
```

--- 

##Other typed reading methods (NSUserDefaults)
```objectivec
arrayForKey:
boolForKey:
dataForKey:
dictionaryForKey:
floatForKey:
integerForKey:
```
---

#Synchronize (NSUserDefaults)

```objectivec
[[NSUserDefaults standardUserDefaults] synchronize];
```

We can use synchronise to ensure all data in memory and disk are in sync.
It is called periodically by the system, so it is not necessary to call it every time we modify NSUserDefaults. Except on unrecoverable scenarios like an application close

--- 

#PRACTICE

---

#plist (Property List)

##Internally it is an XML, but we can edit it by GUI from XCode

---

![](assets/plistExample.png)

---

![](assets/plistCodeExample.png)

---
![](assets/plistCodeExample.png)
#Valid data types (plist)

##`NSArray`, `NSDictionary`, `NSString`, `NSData`, `NSDate` y `NSNumber`

---

#plist operations

##plist on disk, *NSDictionary* in memory

---

#Writing to disk (plist)

```objectivec
NSDictionary *plistData;
NSString *plistFilePath;
[plistData writeToFile:plistFilePath atomically:YES];
```

---

#Reading from disk (plist)

```objectivec
NSURL *plistURL;
NSDictionary *plistData = [NSDictionary 
							dictionaryWithContentsOfURL:plistURL];

NSString *plistFilePath;
NSDictionary *plistData = [NSDictionary 
							dictionaryWithContentsOfFile:plistFilePath];
```

--- 

#PRACTICE

---

#NSFileManager
##High level API for operating with the file system

---

##NSSearchPathForDirectoriesInDomains()
###Returns the base route of our aplication
```objectivec
NSArray * NSSearchPathForDirectoriesInDomains (
	NSSearchPathDirectory directory,    
	NSSearchPathDomainMask domainMask,    
	BOOL expandTilde 
); 
```

---
#Valid NSSearchPathDirectory

- Files that can be *re-created*

`NSCachesDirectory`   (<Application_Home>/Library/Caches)

- *Critical* that can't be re-created

`NSDocumentDirectory` (<Application_Home>/Documents)

---

#Temporary files

```objectivec
NSString *temporaryFilesPath = NSTemporaryDirectory();
```

###<Application_Home>/tmp

---

#Common operations

- Checking if a file exists
- Listing a directory content
- Create a new directory
- Delete a file
- Read a file attributes

---

#Checking if a file exists

```objectivec
NSFileManager *fileManager = [NSFileManager defaultManager];
NSString *documentsPath = [
NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) 
firstObject];
NSString *filePath = [documentsPath stringByAppendingPathComponent:@"file.txt"];
BOOL fileExists = [fileManager fileExistsAtPath:filePath];
```

---

#Listing a directory content

```objectivec
NSFileManager *fileManager = [NSFileManager defaultManager];
NSURL *bundleURL = [[NSBundle mainBundle] bundleURL];
NSArray *contents = [fileManager contentsOfDirectoryAtURL:bundleURL
                               includingPropertiesForKeys:@[]
                                                  options:NSDirectoryEnumerationSkipsHiddenFiles
                                                    error:nil];

NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pathExtension == 'png'"];
for (NSURL *fileURL in [contents filteredArrayUsingPredicate:predicate]) {
    // Enumerate each .png file in directory
}
```
---

#Create a new directory

```objectivec
NSFileManager *fileManager = [NSFileManager defaultManager];
NSString *documentsPath;
NSString *imagesPath = [documentsPath stringByAppendingPathComponent:@"images"];
if (![fileManager fileExistsAtPath:imagesPath]) {
    [fileManager createDirectoryAtPath:imagesPath 
		   withIntermediateDirectories:NO attributes:nil error:nil];
}
```
---

#Delete a file

```objectivec
NSFileManager *fileManager = [NSFileManager defaultManager];
NSString *documentsPath;
NSString *filePath = [documentsPath stringByAppendingPathComponent:@"image.png"];

NSError *error = nil;
if (![fileManager removeItemAtPath:filePath error:&error]) {
    NSLog(@"[Error] %@ (%@)", error, filePath);
}
```
---

#Read a file attributes

```objectivec
NSFileManager *fileManager = [NSFileManager defaultManager];
NSString *documentsPath;
NSString *filePath = [documentsPath stringByAppendingPathComponent:@"Document.pages"];

if ([fileManager fileExistsAtPath:filePath]) {
    NSDictionary *attributes = [fileManager attributesOfItemAtPath:filePath error:nil];
}
```


###`attributes` is an NSSDictionary with all file attributes of `Document.pages`

---

#File Attribute Keys

NSFileType
NSFileSize
NSFileModificationDate
NSFileReferenceCount
NSFileDeviceIdentifier
NSFileOwnerAccountName
NSFileGroupOwnerAccountName 
NSFilePosixPermissions
NSFileSystemNumber
...

--- 

#PRACTICE

