//
//  LoadTVShowsInteractor.m
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "LoadTVShowsInteractor.h"
#import "TVShowProvider.h"

@implementation LoadTVShowsInteractor
- (void)loadShowsWithCompletion:(void(^)(NSArray *shows))completion{
    [self.tvshowsProvider loadShowsWithSuccess:^(NSArray *shows) {
        completion(shows);
    } error:^(NSError *error) {
        completion(nil);
    }];
}
- (TVShowProvider *)tvshowsProvider{
    if (!_tvshowsProvider) {
        _tvshowsProvider = [[TVShowProvider alloc] init];
        _tvshowsProvider.managedObjectContext = self.managedObjectContext;
    }
    return _tvshowsProvider;
}
@end

