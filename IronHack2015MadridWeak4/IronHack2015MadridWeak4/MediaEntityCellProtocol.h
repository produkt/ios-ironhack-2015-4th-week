//
//  MediaEntityCellProtocol.h
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MediaEntityProtocol.h"

@protocol MediaEntityCellProtocol <NSObject>
- (void)drawMediaEntity:(id<MediaEntityProtocol>)mediaEntity;
@end
