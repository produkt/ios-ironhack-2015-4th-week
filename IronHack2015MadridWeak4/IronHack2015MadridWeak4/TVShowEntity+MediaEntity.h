//
//  TVShowEntity+MediaEntity.h
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "TVShowEntity.h"
#import "MediaEntityProtocol.h"

@interface TVShowEntity (MediaEntity)<MediaEntityProtocol>

@end
