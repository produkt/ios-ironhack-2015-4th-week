//
//  TVShowEntity+MediaEntity.m
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "TVShowEntity+MediaEntity.h"
#import "ShowCell.h"

@implementation TVShowEntity (MediaEntity)
- (NSString *)mediaEntityCellReuseIdentifier{
    return NSStringFromClass([ShowCell class]);
}
@end
