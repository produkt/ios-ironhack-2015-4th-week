//
//  MediaViewController.m
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "MediaViewController.h"
#import "LoadTVShowsInteractor.h"
#import "LoadMoviesInteractor.h"
#import "MediaEntityFactory.h"

@interface MediaViewController ()<UITableViewDataSource>
@property (strong,nonatomic) NSMutableArray *items;
@property (strong,nonatomic) LoadMoviesInteractor *loadMoviesInteractor;
@property (strong,nonatomic) LoadTVShowsInteractor *loadTVShowsInteractor;
@end

@implementation MediaViewController
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _items = [NSMutableArray array];
        _loadTVShowsInteractor =[[LoadTVShowsInteractor alloc] init];
        _loadTVShowsInteractor.managedObjectContext = self.managedObjectContext;
        
        _loadMoviesInteractor = [[LoadMoviesInteractor alloc] init];
        _loadMoviesInteractor.managedObjectContext = self.managedObjectContext;
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    dispatch_group_t loadItemsGroup = dispatch_group_create();
    NSMutableArray *items = [NSMutableArray array];
    dispatch_group_enter(loadItemsGroup);
    [self.loadTVShowsInteractor loadShowsWithCompletion:^(NSArray *shows) {
        [items addObjectsFromArray:shows];
        dispatch_group_leave(loadItemsGroup);
    }];
    dispatch_group_enter(loadItemsGroup);
    [self.loadMoviesInteractor loadMoviesWithCompletion:^(NSArray *movies) {
        [items addObjectsFromArray:movies];
        dispatch_group_leave(loadItemsGroup);
    }];
    
    dispatch_group_notify(loadItemsGroup, dispatch_get_main_queue(), ^{
       /// show & movies loaded
        self.items = items;
        [self.view.tableView reloadData];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    id<MediaEntityProtocol> item  =  [self.items objectAtIndex:indexPath.item];
    UITableViewCell<MediaEntityCellProtocol> *cell = [MediaEntityFactory cellForEntity:item forTableView:tableView atIndexPath:indexPath];
    [cell drawMediaEntity:item];
    return cell;
}
@end
