//
//  BaseProvider.m
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "BaseProvider.h"
#import "RequestManagerFactory.h"

@implementation BaseProvider
- (id<RequestManager>)requestManager{
    if (!_requestManager) {
        _requestManager = [RequestManagerFactory requestManager];
        _requestManager.baseDomain = @"https://api.themoviedb.org/3";
        _requestManager.defaultParams = @{@"api_key":@"065b3e195e4a9d2ab69d6a9301cd6201"};
    }
    return _requestManager;
}
@end
