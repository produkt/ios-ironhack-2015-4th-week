//
//  UIImageView+ImageDownloader.m
//  Test
//
//  Created by Daniel García on 26/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "UIImageView+ImageDownloader.h"
#import <objc/runtime.h>
#import "ImageDownloader.h"

static NSString * const operationQueueKey = @"operationQueueKey";

@interface UIImageView (__ImageDownloader)
@property (strong,nonatomic) NSOperationQueue *operationQueue;
@end
@implementation UIImageView (ImageDownloader)
- (NSOperationQueue *)operationQueue{
    NSOperationQueue *operationQueue = objc_getAssociatedObject(self, (__bridge const void *)(operationQueueKey));
    if (!operationQueue) {
        operationQueue = [[NSOperationQueue alloc] init];
        self.operationQueue = operationQueue;
    }
    return operationQueue;
}
- (void)setOperationQueue:(NSOperationQueue *)operationQueue{
    objc_setAssociatedObject(self, (__bridge const void *)(operationQueueKey), operationQueue, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (void)setImageWithURL:(NSURL *)url withCompletion:(void(^)(UIImage *image))completion{
    if (self.operationQueue.operationCount) {
        [self.operationQueue cancelAllOperations];
    }
    self.image = nil;
    ImageDownloadOperation *downloadOperation = [[ImageDownloader defaultDownloader] downloadImageWithURL:url];
    @weakify(downloadOperation);
    [downloadOperation setCompletionBlock:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            @strongify(downloadOperation);
            self.image = downloadOperation.downloadedImage;
            completion(self.image);
        });
    }];
    [self.operationQueue addOperation:downloadOperation];

}
@end
