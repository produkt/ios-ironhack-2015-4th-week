//
//  ImageDownloader.m
//  Test
//
//  Created by Daniel García on 26/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "ImageDownloader.h"

@interface ImageDownloader ()
@property (strong,nonatomic) dispatch_queue_t downloadQueue;
@end
@implementation ImageDownloader
+ (instancetype)defaultDownloader{
    static dispatch_once_t onceToken;
    static ImageDownloader *instance;
    dispatch_once(&onceToken, ^{
        instance = [[ImageDownloader alloc] init];
    });
    return instance;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _downloadQueue = dispatch_queue_create("com.ironhack.image_downloader", DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}
- (NSOperation *)downloadImageWithURL:(NSURL *)url{
    __block ImageDownloadOperation *downloadOperation;
    downloadOperation = [ImageDownloadOperation blockOperationWithBlock:^{
        NSData *cachedImageData = [self cachedImageWithKey:[self imageKeyForURL:url]];
        if (cachedImageData) {
            NSLog(@"Cached Image");
            downloadOperation.downloadedImage = [UIImage imageWithData:cachedImageData];
        }else{
            NSLog(@"Load image %@",[NSDate date]);
            NSData *imageData = [NSData dataWithContentsOfURL:url];
            sleep(1);
            [self writeCacheImageData:imageData withKey:[self imageKeyForURL:url]];
            downloadOperation.downloadedImage = [UIImage imageWithData:imageData];
        }
    }];
    return downloadOperation;
}
- (void)downloadImageWithURL:(NSURL *)url completion:(void(^)(UIImage *image))completion{
    
}
- (void)returnImageWithData:(NSData *)imageData completion:(void(^)(UIImage *image))completion{
    UIImage *image = [UIImage imageWithData:imageData];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"Draw image %@",[NSDate date]);
        completion(image);
    });
}
- (NSData *)cachedImageWithKey:(NSString *)cacheKey{
    NSData *imageData;
    
    for (NSInteger i = 0; i < 100000000; i++) {
        @autoreleasepool {
            [NSDate date];
        }
    }

    
    
    @synchronized(self){
        NSString *cachePath = [[self cacheFolderPath] stringByAppendingPathComponent:cacheKey];
        if ([[NSFileManager defaultManager] fileExistsAtPath:cachePath]) {
            imageData = [NSData dataWithContentsOfFile:cachePath];
        }
    }
    return imageData;
}
- (void)writeCacheImageData:(NSData *)imageData withKey:(NSString *)cacheKey{
    @synchronized(self){
        [imageData writeToFile:[[self cacheFolderPath] stringByAppendingPathComponent:cacheKey] atomically:YES];
    }
}
- (NSString *)cacheFolderPath{
    NSString *appCachesFolder = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSString *cachesFolder = [appCachesFolder stringByAppendingPathComponent:@"images_cache"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:cachesFolder]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:cachesFolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return cachesFolder;
}
- (NSString *)imageKeyForURL:(NSURL *)imageURL{
    // http:\/\/thetvdb.com\/banners\/posters\/81189-22.jpg
    // httpthetvdbcombannersposters81189-22jpg
    
    return [[[[[imageURL description]
               stringByReplacingOccurrencesOfString:@"/" withString:@""]
              stringByReplacingOccurrencesOfString:@":" withString:@""]stringByReplacingOccurrencesOfString:@"\\" withString:@""] stringByReplacingOccurrencesOfString:@"." withString:@""];
}
@end
