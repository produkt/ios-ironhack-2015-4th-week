//
//  ImageDownloadOperation.h
//  Test
//
//  Created by Daniel García on 26/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageDownloadOperation : NSBlockOperation
@property (strong,nonatomic) UIImage *downloadedImage;
@end
