//
//  UIImageView+ImageDownloader.h
//  Test
//
//  Created by Daniel García on 26/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (ImageDownloader)
- (void)setImageWithURL:(NSURL *)url withCompletion:(void(^)(UIImage *image))completion;
@end
