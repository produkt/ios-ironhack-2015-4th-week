//
//  ImageDownloader.h
//  Test
//
//  Created by Daniel García on 26/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageDownloadOperation.h"

@interface ImageDownloader : NSObject
+ (instancetype)defaultDownloader;
- (void)downloadImageWithURL:(NSURL *)url completion:(void(^)(UIImage *image))completion;
- (ImageDownloadOperation *)downloadImageWithURL:(NSURL *)url;
@end
