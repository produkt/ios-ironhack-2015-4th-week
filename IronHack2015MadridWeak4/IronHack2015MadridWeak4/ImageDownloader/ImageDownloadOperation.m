//
//  ImageDownloadOperation.m
//  Test
//
//  Created by Daniel García on 26/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "ImageDownloadOperation.h"

@implementation ImageDownloadOperation
- (void)cancel{
    [self setCompletionBlock:nil];
    [super cancel];
}
@end
