//
//  MediaEntityFactory.m
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "MediaEntityFactory.h"
#import "ShowCell.h"
#import "MovieCell.h"
#import "TVShowEntity+MediaEntity.h"
#import "MovieEntity+MediaEntity.h"

@implementation MediaEntityFactory
+ (UITableViewCell<MediaEntityCellProtocol> *)cellForEntity:(id<MediaEntityProtocol>)entity forTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath{
    return [tableView dequeueReusableCellWithIdentifier:[entity mediaEntityCellReuseIdentifier] forIndexPath:indexPath];
}
+ (void)registerCellsInTableView:(UITableView *)tableView withClass:(Class)class{
    [tableView registerNib:[UINib nibWithNibName:NSStringFromClass(class) bundle:nil] forCellReuseIdentifier:NSStringFromClass(class)];
}
+ (void)registerCellsInTableView:(UITableView *)tableView{
    [self registerCellsInTableView:tableView withClass:[ShowCell class]];
    [self registerCellsInTableView:tableView withClass:[MovieCell class]];
}

@end

