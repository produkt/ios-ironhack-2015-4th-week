//
//  MediaViewController.h
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface MediaViewController : UIViewController
@property (strong,nonatomic) NSManagedObjectContext *managedObjectContext;
@end
