//
//  MediaView.h
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MediaView : UIView
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
