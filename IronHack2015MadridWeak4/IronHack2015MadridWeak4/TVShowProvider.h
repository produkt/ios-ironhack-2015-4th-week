//
//  TVShowProvider.h
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "BaseProvider.h"

@interface TVShowProvider : BaseProvider
- (void)loadShowsWithSuccess:(void(^)(NSArray *shows))successBlock error:(void(^)(NSError *error))errorBlock;
@end
