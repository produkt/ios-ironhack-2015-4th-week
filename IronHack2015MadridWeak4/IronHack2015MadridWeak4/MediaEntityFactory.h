//
//  MediaEntityFactory.h
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaEntityCellProtocol.h"
#import "MediaEntityProtocol.h"

@interface MediaEntityFactory : NSObject
+ (UITableViewCell<MediaEntityCellProtocol> *)cellForEntity:(id<MediaEntityProtocol>)entity forTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;
@end
