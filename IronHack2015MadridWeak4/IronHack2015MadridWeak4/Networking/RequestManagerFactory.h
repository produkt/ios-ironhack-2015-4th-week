//
//  RequestManagerFactory.h
//  Test
//
//  Created by Daniel García on 25/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestManager.h"

@interface RequestManagerFactory : NSObject
+ (id<RequestManager>)requestManager;
@end
