//
//  RequestManagerFactory.m
//  Test
//
//  Created by Daniel García on 25/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "RequestManagerFactory.h"
#import "AFNRequestManager.h"

@implementation RequestManagerFactory
+ (id<RequestManager>)requestManager{
    return [[AFNRequestManager alloc] init];
}
@end
