//
//  RequestManager.h
//  Test
//
//  Created by Daniel García on 25/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SuccessBlock)(id data);
typedef void (^ErrorBlock)(id data,NSError *error);

@protocol RequestManager <NSObject>
@property (copy,nonatomic) NSString *baseDomain;
@property (copy,nonatomic) NSDictionary *defaultParams;
- (void)GET:(NSString *)endpoint parameters:(NSDictionary *)parameters completion:(SuccessBlock)successBlock error:(ErrorBlock)errorBlock;
- (void)POST:(NSString *)endpoint parameters:(NSDictionary *)parameters completion:(SuccessBlock)successBlock error:(ErrorBlock)errorBlock;
@end
