//
//  MovieCell+MediaEntityCell.h
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "MovieCell.h"
#import "MediaEntityCellProtocol.h"

@interface MovieCell (MediaEntityCell)<MediaEntityCellProtocol>

@end
