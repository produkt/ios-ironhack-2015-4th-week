//
//  BaseProvider.h
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestManager.h"
#import <CoreData/CoreData.h>

@interface BaseProvider : NSObject
@property (strong,nonatomic) id<RequestManager> requestManager;
@property (strong,nonatomic) NSManagedObjectContext *managedObjectContext;
@end
