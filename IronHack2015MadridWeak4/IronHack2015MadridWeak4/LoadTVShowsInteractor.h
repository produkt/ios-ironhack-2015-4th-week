//
//  LoadTVShowsInteractor.h
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "BaseInteractor.h"
@class TVShowProvider;

@interface LoadTVShowsInteractor : BaseInteractor
@property (strong,nonatomic) TVShowProvider *tvshowsProvider;
- (void)loadShowsWithCompletion:(void(^)(NSArray *shows))completion;
@end
