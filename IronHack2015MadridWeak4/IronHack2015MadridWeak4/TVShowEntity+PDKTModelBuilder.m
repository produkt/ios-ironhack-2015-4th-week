//
//  TVShowEntity+PDKTModelBuilder.m
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "TVShowEntity+PDKTModelBuilder.h"

@implementation TVShowEntity (PDKTModelBuilder)
+ (NSString *)entityIdPropertyName{
    return @"showId";
}
+ (NSDictionary *)propertiesBindings{
    return @{
             @"showId":@"id",
             @"showTitle":@"original_name",
             @"showPosterURL":@"poster_path",
             };
}
+ (NSDictionary *)propertiesTypeTransformers{
    return @{
             @"showPosterURL":[PDKTURLTransformer new]
             };
}
+ (NSDictionary *)customDataDictionaryWithSourceDataDictionary:(NSDictionary *)dictionary{
    NSMutableDictionary *customData = [dictionary mutableCopy];
    NSString *posterPath = [dictionary objectForKey:@"poster_path"];
    [customData setObject:[@"http://image.tmdb.org/t/p/w500" stringByAppendingString:posterPath] forKey:@"poster_path"];
    return customData;
}
@end
