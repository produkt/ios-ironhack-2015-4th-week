//
//  MovieEntity+PDKTModelBuilder.m
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "MovieEntity+PDKTModelBuilder.h"

@implementation MovieEntity (PDKTModelBuilder)
+ (NSDictionary *)propertiesBindings{
    return @{
             @"movieId":@"id",
             @"movieTitle":@"title",
             @"moviePosterURL":@"poster_path",
             };
}
+ (NSDictionary *)propertiesTypeTransformers{
    return @{
             @"moviePosterURL":[PDKTURLTransformer new]
             };
}
+ (NSDictionary *)customDataDictionaryWithSourceDataDictionary:(NSDictionary *)dictionary{
    NSMutableDictionary *customData = [dictionary mutableCopy];
    NSString *posterPath = [dictionary objectForKey:@"poster_path"];
    [customData setObject:[@"http://image.tmdb.org/t/p/w500" stringByAppendingString:posterPath] forKey:@"poster_path"];
    return customData;
}
@end
