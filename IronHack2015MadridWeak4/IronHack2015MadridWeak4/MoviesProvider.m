//
//  MoviesProvider.m
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "MoviesProvider.h"
#import "MovieEntity+PDKTModelBuilder.h"

@implementation MoviesProvider
- (void)loadMoviesWithSuccess:(void(^)(NSArray *movies))successBlock error:(void(^)(NSError *error))errorBlock{
    [self.requestManager GET:@"/movie/popular" parameters:nil completion:^(id data) {
        NSMutableArray *movies = [NSMutableArray array];
        for (NSDictionary *moviesData in [data valueForKey:@"results"]) {
            MovieEntity *movie = [MovieEntity updateOrInsertIntoManagedObjectContext:self.managedObjectContext withDictionary:moviesData];
            [movies addObject:movie];
        }
        successBlock(movies);
    } error:^(id data, NSError *error) {
        
    }];
}
@end
