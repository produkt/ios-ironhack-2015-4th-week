//
//  TVShowEntity.h
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TVShowEntity : NSManagedObject

@property (nonatomic, retain) NSString * showId;
@property (nonatomic, retain) id showPosterURL;
@property (nonatomic, retain) NSString * showTitle;

@end
