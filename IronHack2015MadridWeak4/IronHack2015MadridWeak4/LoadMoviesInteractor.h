//
//  LoadMoviesInteractor.h
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "BaseInteractor.h"
@class MoviesProvider;

@interface LoadMoviesInteractor : BaseInteractor
@property (strong,nonatomic) MoviesProvider *moviesProvider;
- (void)loadMoviesWithCompletion:(void(^)(NSArray *movies))completion;
@end
