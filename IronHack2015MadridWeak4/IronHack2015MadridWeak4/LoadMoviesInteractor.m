//
//  LoadMoviesInteractor.m
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "LoadMoviesInteractor.h"
#import "MoviesProvider.h"

@implementation LoadMoviesInteractor
- (void)loadMoviesWithCompletion:(void(^)(NSArray *movies))completion{
    [self.moviesProvider loadMoviesWithSuccess:^(NSArray *movies) {
        completion(movies);
    } error:^(NSError *error) {
        completion(nil);
    }];
}
- (MoviesProvider *)moviesProvider{
    if (!_moviesProvider) {
        _moviesProvider = [[MoviesProvider alloc] init];
        _moviesProvider.managedObjectContext = self.managedObjectContext;
    }
    return _moviesProvider;
}
@end
