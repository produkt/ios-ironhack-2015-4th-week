//
//  TVShowProvider.m
//  IronHack2015MadridWeak4
//
//  Created by Daniel García on 27/02/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import "TVShowProvider.h"
#import "TVShowEntity+PDKTModelBuilder.h"

@implementation TVShowProvider
- (void)loadShowsWithSuccess:(void(^)(NSArray *shows))successBlock error:(void(^)(NSError *error))errorBlock{
    [self.requestManager GET:@"/tv/popular" parameters:nil completion:^(id data) {
        NSMutableArray *shows = [NSMutableArray array];
        for (NSDictionary *showData in [data valueForKey:@"results"]) {
            TVShowEntity *show = [TVShowEntity updateOrInsertIntoManagedObjectContext:self.managedObjectContext withDictionary:showData];
            [shows addObject:show];
        }
        successBlock(shows);
    } error:^(id data, NSError *error) {
        
    }];
}
@end
